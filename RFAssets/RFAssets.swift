// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
public typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum Asset {
  public enum Auth {
    public static let authQuestionCircle = ImageAsset(name: "authQuestionCircle")
    public static let backHighlighted = ImageAsset(name: "backHighlighted")
    public static let backNormal = ImageAsset(name: "backNormal")
  }
  public enum Cart {
    public static let cartAddCommentIcon = ImageAsset(name: "cartAddCommentIcon")
    public static let cartEditCommentIcon = ImageAsset(name: "cartEditCommentIcon")
    public static let cartEmptyIcon = ImageAsset(name: "cartEmptyIcon")
  }
  public enum Catalog {
    public static let catalogBuyAgainArrow = ImageAsset(name: "catalogBuyAgainArrow")
    public static let catalogLocationArrow = ImageAsset(name: "catalogLocationArrow")
    public static let catalogLocationPin = ImageAsset(name: "catalogLocationPin")
    public static let catalogFilterSelectorArrow = ImageAsset(name: "catalogFilterSelectorArrow")
    public static let catalogLentaLogo = ImageAsset(name: "catalogLentaLogo")
    public static let catalogMetroLogo = ImageAsset(name: "catalogMetroLogo")
    public static let catalogSparLogo = ImageAsset(name: "catalogSparLogo")
    public static let catalogPromo = ImageAsset(name: "catalogPromo")
    public static let catalogPromo500Friend = ImageAsset(name: "catalogPromo500Friend")
  }
  public enum Categories {
    public static let categoriesAppliance = ImageAsset(name: "categoriesAppliance")
    public static let categoriesBake = ImageAsset(name: "categoriesBake")
    public static let categoriesChild = ImageAsset(name: "categoriesChild")
    public static let categoriesClothes = ImageAsset(name: "categoriesClothes")
    public static let categoriesCoffTea = ImageAsset(name: "categoriesCoffTea")
    public static let categoriesCosmetic = ImageAsset(name: "categoriesCosmetic")
    public static let categoriesDrink = ImageAsset(name: "categoriesDrink")
    public static let categoriesFilterCheck = ImageAsset(name: "categoriesFilterCheck")
    public static let categoriesFishSea = ImageAsset(name: "categoriesFishSea")
    public static let categoriesFrost = ImageAsset(name: "categoriesFrost")
    public static let categoriesGrocery = ImageAsset(name: "categoriesGrocery")
    public static let categoriesHousehold = ImageAsset(name: "categoriesHousehold")
    public static let categoriesMeat = ImageAsset(name: "categoriesMeat")
    public static let categoriesMilk = ImageAsset(name: "categoriesMilk")
    public static let categoriesSport = ImageAsset(name: "categoriesSport")
    public static let categoriesVegFruits = ImageAsset(name: "categoriesVegFruits")
  }
  public enum CategoriesList {
    public static let categoriesListArrow = ImageAsset(name: "categoriesListArrow")
  }
  public enum Checkout {
    public static let checkoutActiveCheck = ImageAsset(name: "checkoutActiveCheck")
    public static let checkoutApplePayLogo = ImageAsset(name: "checkoutApplePayLogo")
    public static let checkoutBusinessLogo = ImageAsset(name: "checkoutBusinessLogo")
    public static let checkoutCardCourierLogo = ImageAsset(name: "checkoutCardCourierLogo")
    public static let checkoutCardLogo = ImageAsset(name: "checkoutCardLogo")
    public static let checkoutDeliveryEmptyIcon = ImageAsset(name: "checkoutDeliveryEmptyIcon")
    public static let checkoutFailureIcon = ImageAsset(name: "checkoutFailureIcon")
    public static let checkoutLinkArrow = ImageAsset(name: "checkoutLinkArrow")
    public static let checkoutLoader = DataAsset(name: "checkoutLoader")
    public static let checkoutMaestroLogo = ImageAsset(name: "checkoutMaestroLogo")
    public static let checkoutMastercardLogo = ImageAsset(name: "checkoutMastercardLogo")
    public static let checkoutMirLogo = ImageAsset(name: "checkoutMirLogo")
    public static let checkoutQRLogo = ImageAsset(name: "checkoutQRLogo")
    public static let checkoutVisaLogo = ImageAsset(name: "checkoutVisaLogo")
    public static let lock = ImageAsset(name: "lock")
  }
  public enum Location {
    public static let current = ImageAsset(name: "current")
    public static let locationActiveCheck = ImageAsset(name: "locationActiveCheck")
    public static let locationClear = ImageAsset(name: "locationClear")
    public static let locationPin = ImageAsset(name: "locationPin")
    public static let locationSearch = ImageAsset(name: "locationSearch")
    public static let minus = ImageAsset(name: "minus")
    public static let plus = ImageAsset(name: "plus")
  }
  public enum Main {
    public static let tabBarCartActive = ImageAsset(name: "tabBarCartActive")
    public static let tabBarCartInactive = ImageAsset(name: "tabBarCartInactive")
    public static let tabBarHomeActive = ImageAsset(name: "tabBarHomeActive")
    public static let tabBarHomeInactive = ImageAsset(name: "tabBarHomeInactive")
    public static let tabBarProfileActive = ImageAsset(name: "tabBarProfileActive")
    public static let tabBarProfileInactive = ImageAsset(name: "tabBarProfileInactive")
    public static let tabBarSearchActive = ImageAsset(name: "tabBarSearchActive")
    public static let tabBarSearchInactive = ImageAsset(name: "tabBarSearchInactive")
    public static let animatedLoader = DataAsset(name: "animatedLoader")
    public static let mainTransparentLogo = ImageAsset(name: "mainTransparentLogo")
    public static let noPhoto = ImageAsset(name: "noPhoto")
    public static let noPhotoDark = ImageAsset(name: "noPhotoDark")
    public static let textFieldClear = ImageAsset(name: "textFieldClear")
  }
  public enum Networks {
    public static let networkLocationArrow = ImageAsset(name: "networkLocationArrow")
    public static let networkLocationPin = ImageAsset(name: "networkLocationPin")
    public static let networkCheck = ImageAsset(name: "networkCheck")
    public static let networkLentaCover = ImageAsset(name: "networkLentaCover")
    public static let networkMetroCover = ImageAsset(name: "networkMetroCover")
    public static let networkSparCover = ImageAsset(name: "networkSparCover")
  }
  public enum Product {
    public static let productBigMinusActive = ImageAsset(name: "productBigMinusActive")
    public static let productBigMinusInactive = ImageAsset(name: "productBigMinusInactive")
    public static let productBigPlusActive = ImageAsset(name: "productBigPlusActive")
    public static let productBigPlusInactive = ImageAsset(name: "productBigPlusInactive")
    public static let productBigTrashActive = ImageAsset(name: "productBigTrashActive")
    public static let productBigTrashInactive = ImageAsset(name: "productBigTrashInactive")
    public static let productMinusActive = ImageAsset(name: "productMinusActive")
    public static let productMinusInactive = ImageAsset(name: "productMinusInactive")
    public static let productPlusActive = ImageAsset(name: "productPlusActive")
    public static let productPlusInactive = ImageAsset(name: "productPlusInactive")
    public static let productTrashActive = ImageAsset(name: "productTrashActive")
    public static let productTrashInactive = ImageAsset(name: "productTrashInactive")
  }
  public enum Products {
    public static let filter = ImageAsset(name: "filter")
  }
  public enum Profile {
    public static let profileAbout = ImageAsset(name: "profileAbout")
    public static let profileArrow = ImageAsset(name: "profileArrow")
    public static let profileBusiness = ImageAsset(name: "profileBusiness")
    public static let profileEdit = ImageAsset(name: "profileEdit")
    public static let profileHelp = ImageAsset(name: "profileHelp")
    public static let profileInvite = ImageAsset(name: "profileInvite")
    public static let profileLocation = ImageAsset(name: "profileLocation")
    public static let profileNotifications = ImageAsset(name: "profileNotifications")
    public static let profileOrders = ImageAsset(name: "profileOrders")
    public static let profileSupport = ImageAsset(name: "profileSupport")
  }
  public enum ProfileBusiness {
    public static let profileBusinessCheckboxActive = ImageAsset(name: "profileBusinessCheckboxActive")
    public static let profileBusinessCheckboxInactive = ImageAsset(name: "profileBusinessCheckboxInactive")
  }
  public enum ProfileOrders {
    public static let profileOrdersArrow = ImageAsset(name: "profileOrdersArrow")
    public static let profileOrdersDoc = ImageAsset(name: "profileOrdersDoc")
    public static let profileOrdersDownload = ImageAsset(name: "profileOrdersDownload")
    public static let profileOrdersLittleStar = ImageAsset(name: "profileOrdersLittleStar")
    public static let profileOrdersLittleStarFilled = ImageAsset(name: "profileOrdersLittleStarFilled")
    public static let profileOrdersPhone = ImageAsset(name: "profileOrdersPhone")
    public static let profileOrdersStar = ImageAsset(name: "profileOrdersStar")
    public static let profileOrdersStarFilled = ImageAsset(name: "profileOrdersStarFilled")
    public static let profileOrdersStarInitital = ImageAsset(name: "profileOrdersStarInitital")
  }
  public enum ProfileOrdersDetail {
    public static let profileOrdersDetailCheck = ImageAsset(name: "profileOrdersDetailCheck")
    public static let profileOrdersDetailCompleteCheck = ImageAsset(name: "profileOrdersDetailCompleteCheck")
    public static let profileOrdersDetailCross = ImageAsset(name: "profileOrdersDetailCross")
    public static let profileOrdersDetailInfoCommentArrow = ImageAsset(name: "profileOrdersDetailInfoCommentArrow")
    public static let profileOrdersDetailLittleStar = ImageAsset(name: "profileOrdersDetailLittleStar")
    public static let profileOrdersDetailLittleStarFilled = ImageAsset(name: "profileOrdersDetailLittleStarFilled")
    public static let profileOrdersDetailProductCommentArrow = ImageAsset(name: "profileOrdersDetailProductCommentArrow")
    public static let profileOrdersDetailWarningCross = ImageAsset(name: "profileOrdersDetailWarningCross")
  }
  public enum Search {
    public static let searchBarcode = ImageAsset(name: "searchBarcode")
    public static let searchFlash = ImageAsset(name: "searchFlash")
  }
  public enum SignChoosing {
    public static let signChoosingCover = ImageAsset(name: "signChoosingCover")
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

public struct DataAsset {
  public fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(macOS)
  @available(iOS 9.0, macOS 10.11, *)
  public var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(macOS)
@available(iOS 9.0, macOS 10.11, *)
public extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(macOS)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

public struct ImageAsset {
  public fileprivate(set) var name: String

  #if os(macOS)
  public typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  public typealias Image = UIImage
  #endif

  public var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image named \(name).")
    }
    return result
  }
}

public extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
